$(function(){
	// ======================= Init Summernote
    $('#editor').summernote({
        placeholder: 'Happy documenting',
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            // ['link', ['linkDialogShow', 'unlink']],
            ['table', ['table']],
            ['insert', ['link', 'picture']],
            ['codeview', ['codeview']],
            ['help', ['help']],
        ],
        height:$(document).height() - 167 ,
        // callbacks: {
        //     onChange: function(contents) {
        //         $('#viewer').html(contents)
        //     }
        // }
    });
    $('.staticBotMenu').show();
    // ASSESS control modal select
    $(".accessability_dropdown li a").click(function(){
        $('.active_access').html($(this).closest('li').html());
        $('.active_access_cont').show()
    });

    // Location Modal
    $('.browse_treeBtn').click(function(e){
        e.preventDefault();
        if(!$(this).hasClass('active_option')){
            $(this).addClass('active_option');
            $('.search_locationBtn').removeClass('active_option');
        }
        $('._search_doc').hide()
        $('._browse_tree').show()
    })
     $('.search_locationBtn').click(function(e){
        e.preventDefault();
        if(!$(this).hasClass('active_option')){
            $(this).addClass('active_option');
            $('.browse_treeBtn').removeClass('active_option');
        }
        $('._browse_tree').hide()
        $('._search_doc').show()
    })
})